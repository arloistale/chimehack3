/**
 * Created by jonathanlu on 8/27/16.
 */

'use strict';

const
    bodyParser = require('body-parser'),
    config = require('config'),
    crypto = require('crypto'),
    express = require('express'),
    https = require('https'),
    request = require('request');

const TYPE_INVALID = 0;
const TYPE_BUTTONS = 1;
const TYPE_QUICK_REPLY = 2;
const TYPE_CONTINUE = 3;
const TYPE_TEXT = 4;
const TYPE_GENERIC = 5;
const TYPE_GENERIC_CONTINUE = 6;

var messageSender = (function() {
    var SERVER_URL, PAGE_ACCESS_TOKEN;

    const messages = {
        entry: {
            type: TYPE_CONTINUE,
            prompt: "Hello there, I'm glad you're here right now. This is a safe place to talk. Your questions and identity are kept anonymous, confidential, and will not be shared.",
            continue: "areYouSafe"
        },
        areYouSafe: {
            type: TYPE_QUICK_REPLY,
            prompt: "Are you safe now?",
            replies: [
                {
                    title: "Yes",
                    callback: "medicalAttention"
                },
                {
                    title: "No",
                    callback: "end"
                }
            ]
        },
        medicalAttention: {
            type: TYPE_QUICK_REPLY,
            prompt: "Do you need medical attention now?",
            replies: [
                {
                    title: "Yes",
                    callback: "end"
                },
                {
                    title: "No",
                    callback: "moreInfo"
                }
            ]
        },
        emergencyCall: {
            type: TYPE_BUTTONS,
            prompt: "You need help.",
            buttons: [
                {
                    title: "Call 911",
                    payload: "911"
                }
            ]
        },
        suicidalCall: {
            type: TYPE_BUTTONS,
            prompt: "Suicide Prevention Line",
            buttons: [
                {
                    type: "phone_number",
                    title: "1-800-273-8255",
                    payload: "+18002738255"
                }
            ]
        },
        moreInfo: {
            type: TYPE_QUICK_REPLY,
            prompt: "Would you like to tell me what happened?",
            replies: [
                {
                    title: "Yes",
                    callback: "elaborate"
                },
                {
                    title: "No",
                    callback: "end"
                }
            ]
        },
        elaborate: {
            type: TYPE_QUICK_REPLY,
            prompt: "Can you elaborate more about what happened?",
            replies: [
                {
                    title: "Raped",
                    callback: "notYourFault"
                },
                {
                    title: "Injured",
                    callback: "end"
                },
                {
                    title: "Neither",
                    callback: "notYourFault"
                }
            ]
        },
        notYourFault: {
            type: TYPE_QUICK_REPLY,
            prompt: "What happened was not your fault. You are not alone. I am here to help you. How are you feeling right now?",
            replies: [
                {
                    title: "I wanna chat",
                    callback: "happenTime"
                },
                {
                    title: "I need urgent help",
                    callback: "suicidalCall"
                }
            ]
        },
        happenTime: {
            type: TYPE_CONTINUE,
            prompt: "I see. It is perfectly normal to feel the way you are feeling right now. In most cases, evidence collected within 72 hours of the incident is the most valuable.",
            continue: "report"
        },
        report: {
            type: TYPE_QUICK_REPLY,
            prompt: "Would you like to know how to report this?",
            replies: [
                {
                    title: "Yes",
                    callback: "reinforcement"
                },
                {
                    title: "No",
                    callback: "end"
                }
            ]
        },
        reinforcement: {
            type: TYPE_CONTINUE,
            prompt: "That's a very brave thing to do. We will guide you through the process.",
            continue: "avoidActivitiesText"
        },
        avoidActivitiesText: {
            type: TYPE_CONTINUE,
            prompt: "If you are able to, try to avoid activities that could potentially damage evidence such as:",
            continue: "avoidActivitiesImage"
        },
        avoidActivitiesImage: {
            type: TYPE_GENERIC_CONTINUE,
            title: "Avoid Activities",
            imageURL: "/assets/1.png",
            continue: "saveClothing"
        },
        saveClothing: {
            type: TYPE_QUICK_REPLY,
            prompt: "Save all of the clothing you were wearing at the time of the assault. Place each item of clothing in a separate paper bag. Do not use plastic bags.",
            replies: [
                {
                    title: "OK",
                    callback: "medicalCare"
                }
            ]
        },
        medicalCare: {
            type: TYPE_QUICK_REPLY,
            prompt: "Get medical care as soon as possible. Sexual assault can impact your physical and emotional health. You may have injuries and trauma related to the assaults that are not immediately visible.",
            replies: [
                {
                    title: "OK",
                    callback: "nearbyText"
                }
            ]
        },
        nearbyText: {
            type: TYPE_CONTINUE,
            prompt: "Here are nearby Emergency Centers.",
            continue: "nearbyImage"
        },
        nearbyImage: {
            type: TYPE_GENERIC_CONTINUE,
            title: "Nearby Emergency Centers",
            imageURL: "/assets/2.png",
            continue: "contactFriend"
        },
        contactFriend: {
            type: TYPE_QUICK_REPLY,
            prompt: "Would you like me to contact close friends or family to support you at this time?",
            replies: [
                {
                    title: "Yes",
                    callback: "callFriendFamily"
                },
                {
                    title: "No",
                    callback: "epilogue"
                }
            ]
        },
        callFriendFamily: {
            type: TYPE_BUTTONS,
            prompt: "Friends",
            buttons: [
                {
                    type: "phone_number",
                    title: "Jonathan",
                    payload: "+14157131058"
                }
            ]
        },
        epilogue: {
            type: TYPE_QUICK_REPLY,
            prompt: "Do you need any more help?",
            replies: [
                {
                    title: "Yes",
                    callback: "end"
                },
                {
                    title: "No",
                    callback: "conclusion"
                }
            ]
        },
        conclusion: {
            type: TYPE_TEXT,
            content: "I hope this was helpful. Please feel free to come back to chat. I'm here for you any time!"
        },
        end: {
            type: TYPE_BUTTONS,
            prompt: "Helpline Numbers",
            buttons: [
                {
                    type: "phone_number",
                    title: "National Sexual Assault Hotline",
                    payload: "+18006564673"
                },
                {
                    type: "phone_number",
                    title: "National Domestic Violence",
                    payload: "+18007873224"
                }
            ]
        }
    };

    /*
     * Call the Send API. The message data goes in the body. If successful, we'll
     * get the message id in a response
     *
     */
    var callSendAPI = function(messageData) {
        request({
            uri: 'https://graph.facebook.com/v2.6/me/messages',
            qs: { access_token: PAGE_ACCESS_TOKEN },
            method: 'POST',
            json: messageData

        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var recipientId = body.recipient_id;
                var messageId = body.message_id;

                if (messageId) {
                    console.log("Successfully sent message with id %s to recipient %s",
                        messageId, recipientId);
                } else {
                    console.log("Successfully called Send API for recipient %s",
                        recipientId);
                }
            } else {
                console.error(response.error);
            }
        });
    };

    return {
        messages: messages,

        init: function(serverURL, pageToken) {
            console.log("Message Sender initialized");
            SERVER_URL = serverURL;
            PAGE_ACCESS_TOKEN = pageToken;
        },

        sendResponse: function(recipientId, response) {
            switch(response.type) {
                case TYPE_QUICK_REPLY:
                    this.sendQuickReply(recipientId, response.prompt, response.replies);
                    break;
                case TYPE_BUTTONS:
                    this.sendEmergencyButtonMessage(recipientId, response.prompt, response.buttons);
                    break;
                case TYPE_CONTINUE:
                    this.sendTextMessage(recipientId, response.prompt);

                    var cont = messages[response.continue];
                    this.sendDelayedResponse(recipientId, cont);
                    break;
                case TYPE_TEXT:
                    this.sendTextMessage(recipientId, response.content);
                    break;
                case TYPE_GENERIC:
                    this.sendGenericMessage(recipientId, response.title, response.imageURL);
                    break;
                case TYPE_GENERIC_CONTINUE:
                    this.sendGenericMessage(recipientId, response.title, response.imageURL);

                    var cont2 = messages[response.continue];
                    this.sendDelayedResponse(recipientId, cont2);
                    break;
            }
        },

        /*
         * Currently delay is set to 1 second.
         */
        sendDelayedResponse: function(recipientId, response) {
            var that = this;

            setTimeout(function() {
                that.sendResponse(recipientId, response);
            }, 1200);
        },

        /*
         * Send an image using the Send API.
         *
         */
        sendImageMessage: function(recipientId) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "image",
                        payload: {
                            url: SERVER_URL + "/assets/rift.png"
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a Gif using the Send API.
         *
         */
        sendGifMessage: function(recipientId) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "image",
                        payload: {
                            url: SERVER_URL + "/assets/instagram_logo.gif"
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send audio using the Send API.
         *
         */
        sendAudioMessage: function(recipientId) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "audio",
                        payload: {
                            url: SERVER_URL + "/assets/sample.mp3"
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a video using the Send API.
         *
         */
        sendVideoMessage: function(recipientId) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "video",
                        payload: {
                            url: SERVER_URL + "/assets/allofus480.mov"
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a video using the Send API.
         *
         */
        sendFileMessage: function(recipientId) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "file",
                        payload: {
                            url: SERVER_URL + "/assets/test.txt"
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a text message using the Send API.
         *
         */
        sendTextMessage: function(recipientId, messageText) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    text: messageText,
                    metadata: "DEVELOPER_DEFINED_METADATA"
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a button message using the Send API.
         *
         */
        sendEmergencyButtonMessage: function(recipientId, promptText, buttons) {
            var buttons_arr = [];

            for(var i = 0; i < buttons.length; ++i) {
                buttons_arr.push({
                    type: buttons[i].type,
                    title: buttons[i].title,
                    payload: buttons[i].payload
                })
            }

            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "template",
                        payload: {
                            template_type: "button",
                            text: promptText,
                            buttons: buttons_arr
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a Structured Message (Generic Message type) using the Send API.
         *
         */
        sendGenericMessage: function(recipientId, title, imageURL) {
            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    attachment: {
                        type: "template",
                        payload: {
                            template_type: "generic",
                            elements: [{
                                title: title,
                                image_url: SERVER_URL + imageURL
                            }]
                        }
                    }
                }
            };

            callSendAPI(messageData);
        },

        /*
         * Send a message with Quick Reply buttons.
         * Give promptText string, and replies array of data
         * Refer to API reference for more info.
         */
        sendQuickReply: function(recipientId, promptText, replies) {
            var qk_reply = [];
            for (var i = 0; i < replies.length; i++) { 
                var obj = {
                    content_type: "text", 
                    title: replies[i].title,
                    payload: replies[i].callback
                };
                qk_reply[i] =  obj;
            }

            var messageData = {
                recipient: {
                    id: recipientId
                },
                message: {
                    text: promptText,
                    metadata: "DEVELOPER_DEFINED_METADATA",
                    quick_replies: qk_reply
                }
            };

            callSendAPI(messageData);
        }
    };
})();

module.exports = messageSender;